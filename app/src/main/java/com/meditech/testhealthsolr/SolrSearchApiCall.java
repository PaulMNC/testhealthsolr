package com.meditech.testhealthsolr;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class to handle SOLR searches
 * Created by Parents on 11/2/2017.
 */

class SolrSearchApiCall extends AsyncTask<Object, Void, String> {
    private static final String TAG = "DoctorM";

    interface AsyncResponse {
        void processFinish(String output);
    }

    private AsyncResponse delegate = null;

    SolrSearchApiCall(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    public String doInBackground(Object... args) {
        String host = "https://atet-api.meditech.com:8443/";
        String root = "v1/freetext/search/";
        String database = "dictionaries";
        String queryPrefix = "query=";
        String seed = Uri.encode((String) args[0]);

        JSONObject jsonDoc = new JSONObject();
        JSONObject params = new JSONObject();
        JSONObject filter = new JSONObject();

        String queryFields = Uri.encode("title^1.5 content");
        String defType = "edismax";
        String responseWriter = "json";

        try {
            params.put("defType", defType);
            params.put("start", 0);
            params.put("rows", 10);
            params.put("qf", queryFields);
            params.put("wt", responseWriter);

            filter.put("subsetId", "Medications");
            filter.put("docType", "PhaName");

            jsonDoc.put("params", params);
            jsonDoc.put("query", seed);
            jsonDoc.put("filter", filter);

            //jsonDoc.put("resouce", "v1/resource/freetext-search-request/_version/1/");

        } catch (JSONException e) {
            Log.e(TAG, "JSONException: " + e.toString());
        }

        String path = host + root + database + "/?" + queryPrefix + jsonDoc;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String errorText = "ERROR";
        Log.i(TAG, "path: " + path);
        try {
            URL url = new URL(path);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);

            int responseCode = connection.getResponseCode();
            Log.i(TAG, "response code  = " + responseCode + "   response message  = " + connection.getResponseMessage());
            if (responseCode == 202 || responseCode == 200) {
                inputStream = connection.getInputStream();
                @SuppressWarnings("RedundantStringConstructorCall")
                String answer = IStoString(inputStream);
                return answer;
            } else {
                Log.e(TAG, "Error");
                return errorText;
            }
        }
        catch (IOException e) {
            Log.e(TAG, "IOException: " + e.toString());
            return errorText;
        }
        catch (Exception e) {
            Log.e(TAG, "Exception: " + e.toString());
            return errorText;
        }

        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "couldn't close InputStream");
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public void onPostExecute(String result) {
        if (result.startsWith("ERROR")) {
            Log.e(TAG, "onPostExecute result = " + result);
        } else {
            if (delegate != null) {
                delegate.processFinish(result);
            }
        }
    }

    private static String IStoString(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
