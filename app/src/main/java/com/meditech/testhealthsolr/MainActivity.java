package com.meditech.testhealthsolr;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "TestHealthSolr";
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private Menu optionsMenu;

    enum Sources {
        PhaName, FDBOrderText, AmbOrderSet
    }
    private Sources source = Sources.FDBOrderText;

    private int page = 0;
    private int rows = 10;
    private String voiceTextResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptSpeechInput();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.pha_med_name_source:
                changeSelectedSource(item, Sources.PhaName);
                return true;
            case R.id.fdb_order_text_source:
                changeSelectedSource(item, Sources.FDBOrderText);
                return true;
            case R.id.amb_ord_set_source:
                changeSelectedSource(item, Sources.AmbOrderSet);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeSelectedSource(MenuItem item, Sources newSource) {
        if (source != newSource) {
            clearSourceCheckbox(source);
            item.setChecked(true);
            source = newSource;
        }
    }

    private void clearSourceCheckbox(Sources source) {
        switch (source) {
            case PhaName:
                optionsMenu.findItem(R.id.pha_med_name_source).setChecked(false);
                break;
            case FDBOrderText:
                optionsMenu.findItem(R.id.fdb_order_text_source).setChecked(false);
                break;
            case AmbOrderSet:
                optionsMenu.findItem(R.id.amb_ord_set_source).setChecked(false);
                break;
        }
    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    voiceTextResult = result.get(0);
                    TextView voiceResultTextView = (TextView) findViewById(R.id.voiceToTextResult);
                    voiceResultTextView.setText(voiceTextResult);
                    Log.i(TAG, "speech detected: " + voiceTextResult);
                    page = 0;
                    searchSolr(voiceTextResult, source, rows, page * rows);
                }
            }
        }
    }

    void searchSolr(String seed, Sources source, int rows, int start) {
        new SolrSearchCall(asyncResponse())
                .execute(seed, source, rows, start);
    }

    private SolrSearchCall.AsyncResponse asyncResponse() {
        final TextView voiceResultTextView = (TextView) findViewById(R.id.voiceToTextResult);
        final TextView searchResultsTextView = (TextView) findViewById(R.id.searchResults);
        final TextView moreButton = (TextView) findViewById(R.id.moreButton);
        final int currentPage = page;
        return new SolrSearchCall.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //Log.d(TAG, "processFinish: " + output);
                String results = "";
                String line;
                if (output.startsWith("ERROR")) {
                    voiceResultTextView.setText(output);
                } else {
                    JSONObject jsonObject;
                    JSONArray jsonArray;
                    try {
                        jsonObject = new JSONObject(output);
                        JSONObject responseHeader = jsonObject.optJSONObject("responseHeader");
                        voiceResultTextView.setText(getSearchQuery(responseHeader));
                        JSONObject response = jsonObject.optJSONObject("response");
                        setMoreButtonVisibility(moreButton, response);
                        if (currentPage > 0) {
                            results = searchResultsTextView.getText().toString();
                        }
                        jsonArray = response.optJSONArray("docs");
                        for (int index = 0; index < jsonArray.length(); index++) {
                            line = jsonArray.getJSONObject(index).optString("title");
                            if (line.length() > 0) {
                                results = results + line + "\n";
                            }
                        }
                        if (results.isEmpty()) results = "No results found";
                        searchResultsTextView.setText(results);
                    } catch (JSONException e) {
                        Log.e(TAG, "searchSolr JSONException: " + e.toString());
                    }
                }
            }
        };
    }

    private String getSearchQuery(JSONObject responseHeader) {
        JSONObject params = responseHeader.optJSONObject("params");
        return params.optString("q");
    }

    private void setMoreButtonVisibility(TextView moreButton, JSONObject response) {
        int numFound = response.optInt("numFound");
        if (numFound > (page + 1) * rows) {
            moreButton.setVisibility(View.VISIBLE);
        } else {
            moreButton.setVisibility(View.GONE);
        }
    }

    public void moreOnClick(View view) {
        Log.i(TAG, "moreOnClick");
        page++;
        searchSolr(voiceTextResult, source, rows, page * rows);
    }

//    void searchSolrApi(String seed) {
//        new MeditechApiCall(asyncResponse())
//                .execute("medSearch", seed);
//    }
//
//    private MeditechApiCall.AsyncResponse asyncResponse() {
//        return new MeditechApiCall.AsyncResponse() {
//            @Override
//            public void processFinish(String output) {
//            }
//        };
//    }

    //    void searchSolr(String seed) {
//        //final TextView responseTextTextView = (TextView) findViewById(R.id.responseText);
//        new SolrSearchApiCall(new SolrSearchApiCall.AsyncResponse() {
//            @Override
//            public void processFinish(String output) {
//                Log.d(TAG, "processFinish: " + output);
//                String results = "";
//                String line;
//                if (!output.startsWith("ERROR")) {
//                    JSONObject jsonObject;
//                    JSONArray jsonArray;
//                    try {
//                        jsonObject = new JSONObject(output);
//                        jsonObject = jsonObject.optJSONObject("response");
//                        jsonArray = jsonObject.optJSONArray("docs");
//                        for (int index = 0; index < jsonArray.length(); index++) {
//                            line = jsonArray.getJSONObject(index).optString("title");
//                            if (line.length() > 0) {
//                                results = results + line + "\n";
//                            }
//                        }
//                        //responseTextTextView.setText(results);
//                        Log.d(TAG, "results: " + results);
//                    } catch (JSONException e) {
//                        Log.d(TAG, "searchSolr JSONException: " + e.toString());
//                    }
//                }
//            }
//        }).execute(seed);
//    }
//    public class OverScrollNextPage extends ScrollView {
//
//        @Override
//        protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
//            super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
//            Log.d(TAG, "onOverScrolled: " + scrollX + "-" + scrollY + "-" + clampedX + "-" +
//                    clampedY);
//        }
//    }
//
//    public interface OnOverScrolledListener {
//    void onOverScrolled(ScrollView scrollView, int deltaX, int deltaY, boolean clampedX,
//                        boolean clampedY);
//    }
//
//    private  OnOverScrolledListener onOverScrolledListener;

}
