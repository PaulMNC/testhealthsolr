package com.meditech.testhealthsolr;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Class for the API to get data from MEDITECH system
 * Created by PNIEBUHR on 10/4/2017.
 */

class MeditechApiCall extends AsyncTask<Object, Void, String> {
    private static final String TAG = "DoctorM";

    interface AsyncResponse {
        void processFinish(String output);
    }

    private AsyncResponse delegate = null;

    MeditechApiCall(AsyncResponse delegate) {
        this.delegate = delegate;
    }


    public String doInBackground(Object... args) {
        String type = (String) args[0];
        InputStream inputStream = null;
        HttpsURLConnection conn = null;
        String bearerToken = "oQr4FcIFQae2qlc770mKzg==";
        String host = "https://atet-api.meditech.com:8443/";
        JSONObject jsonDoc = new JSONObject();
        String path;

        String errorText = "error";

        switch (type) {
            case "medSearch":
                path = host + "v1/freetext/search/EmrRecords";

                JSONObject params = new JSONObject();
                JSONObject filter = new JSONObject();
                String seed = (String) args[1];

                try {
                    params.put("defType", "edismax");
                    params.put("start", 0);
                    params.put("rows", 10);
                    params.put("qf", Uri.encode("title^1.5 content"));
                    params.put("wt", "json");

                    filter.put("subsetId", "Medications");
                    filter.put("docType", "PhaName");

                    jsonDoc.put("params", params);
                    jsonDoc.put("query", seed);
                    jsonDoc.put("filter", filter);

                    //jsonDoc.put("resouce", "v1/resource/freetext-search-request/_version/1/");

                } catch (JSONException e) {
                    Log.e(TAG, "JSONException: " + e.toString());
                }
                break;
            default:
                return "Unrecognized API call";
        }

        try {
            URL url = new URL(path);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("authorization", "Bearer " + bearerToken);
            conn.setRequestProperty("content-type", "application/json");
            conn.setDoOutput(true);

            DataOutputStream sendStream = new DataOutputStream(conn.getOutputStream());
            sendStream.writeBytes(jsonDoc.toString());
            sendStream.flush();
            sendStream.close();

            //conn.connect();
            int responseCode = conn.getResponseCode();
            Log.i(TAG, "response code  = " + responseCode + "   response message  = " + conn.getResponseMessage());
            if (responseCode == 202 || responseCode == 200) {
                inputStream = conn.getInputStream();
                @SuppressWarnings("RedundantStringConstructorCall")
                String answer = IStoString(inputStream);
                return answer;
            } else {
                return errorText;
            }
        }
        catch (IOException e) {
            Log.e(TAG, "IOException: " + e.toString());
            return errorText;
        }
        catch (Exception e) {
            Log.e(TAG, "Exception: " + e.toString());
            return errorText;
        }

        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "couldn't close InputStream");
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public void onPostExecute(String result) {
        if (result.startsWith("error")) {
            Log.e(TAG, "onPostExecute result = " + result);
        } else {
            if (delegate != null) {
                delegate.processFinish(result);
            }
        }
    }

    private static String IStoString(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
