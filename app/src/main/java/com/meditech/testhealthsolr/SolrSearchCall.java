package com.meditech.testhealthsolr;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class to handle SOLR searches
 * Created by Paul Niebuhr on 11/2/2017.
 */

class SolrSearchCall extends AsyncTask<Object, Void, String> {
    private static final String TAG = "DoctorM";

    interface AsyncResponse {
        void processFinish(String output);
    }

    private AsyncResponse delegate = null;

    SolrSearchCall(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    public String doInBackground(Object... args) {
        String seed = Uri.encode((String) args[0]);
        //String seedString = (String) args[0];
        //String seed = Uri.encode(seedString.replace(" ", " AND "));
        MainActivity.Sources source = (MainActivity.Sources) args[1];
        int rows = (int) args[2];
        int start = (int) args[3];
        String hostRoot = "http://stxsrch.meditech.com:8981/solr/ATET/select?";
        String queryFields = Uri.encode("title^1.5 content");
        String filterQueryDocType = null;
        String filterQuerySubsetID = null;
        switch (source) {
            case FDBOrderText:
                filterQuerySubsetID = "subsetId:Medications";
                filterQueryDocType = "docType:FDB_FULL_ORDER_TEXT";
                break;
            case PhaName:
                filterQuerySubsetID = "subsetId:Medications";
                filterQueryDocType = "docType:PhaName";
                break;
            case AmbOrderSet:
                filterQuerySubsetID = "subsetId:OrderSet";
                filterQueryDocType = "docType:AmbOrdSet";
                break;
        }
        String defType = "edismax";
        String responseWriter = "json";

        String hostPath = hostRoot +
                "q=" + seed +
                "&qf=" + queryFields +
                "&rows=" + rows +
                "&start=" + start +
                "&defType=" + defType +
                "&wt=" + responseWriter +
                "&fq=" + filterQueryDocType +
                "&fq=" + filterQuerySubsetID +
                "&q.op=AND";
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String errorText = "ERROR";
        Log.i(TAG, "hostPath: " + hostPath);
        try {
            URL url = new URL(hostPath);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);

            int responseCode = connection.getResponseCode();
            Log.i(TAG, "response code  = " + responseCode + "   response message  = " + connection.getResponseMessage());
            if (responseCode == 202 || responseCode == 200) {
                inputStream = connection.getInputStream();
                @SuppressWarnings("RedundantStringConstructorCall")
                String answer = IStoString(inputStream);
                return answer;
            } else {
                Log.e(TAG, "Error");
                return errorText;
            }
        }
        catch (IOException e) {
            Log.e(TAG, "IOException: " + e.toString());
            return errorText;
        }
        catch (Exception e) {
            Log.e(TAG, "Exception: " + e.toString());
            return errorText;
        }

        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "couldn't close InputStream");
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public void onPostExecute(String result) {
        if (result.startsWith("ERROR")) {
            Log.e(TAG, "onPostExecute result = " + result);
        } else {
            Log.i(TAG, "oPE: " + result);
            if (delegate != null) {
                delegate.processFinish(result);
            }
        }
    }

    private static String IStoString(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
